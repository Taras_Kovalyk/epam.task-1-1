﻿using System;

namespace Task1b
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                DemonstrateWorkOfProgram();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void DemonstrateWorkOfProgram()
        {
            Console.WriteLine("Demonstrating of work:");
            Console.WriteLine("*****************************");
            Console.WriteLine("Creating new rectangle in (0,0) with sides 3 and 4");
            var firstRectangle = new Rectangle(0, 0, 3, 4);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Moving firstRectangle on 5 points right");
            firstRectangle.Move(5, Direction.Right);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Moving firstRectangle on 3 points up");
            firstRectangle.Move(3, Direction.Up);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Scaling firstRectangle in 2 points with fixed left down point");
            firstRectangle.SideScale(2);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Scaling firstRectangle in 2 points with fixed center");
            firstRectangle.CenterScale(2);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Scaling to previous size (scale coefficient = 0.5)");
            firstRectangle.CenterScale(0.5);
            Console.WriteLine(firstRectangle + Environment.NewLine);

            Console.WriteLine("Creating two another rectangles");
            var secondRectangle = new Rectangle(-4, 6, 5, 10);
            var thirdRectangle = new Rectangle(-2, 4, 2, 8);
            Console.WriteLine(secondRectangle);
            Console.WriteLine(thirdRectangle);
            Console.WriteLine();

            Console.WriteLine("Rectangle in their intersection");
            Console.WriteLine(secondRectangle.Intersect(thirdRectangle) + Environment.NewLine);

            Console.WriteLine("Trying to intersect two rectangles that do not intersect");
            try
            {
                Console.WriteLine(firstRectangle.Intersect(secondRectangle));
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(Environment.NewLine + "The least retangle that contains secondRectangle and thirdRectangle");
            Console.WriteLine(secondRectangle.FindTheLeastRectangle(thirdRectangle));
        }
    }
}
