﻿using System;

namespace Task1c
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                DemonstrateWorkOfProgram();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadLine();
        }

        public static void DemonstrateWorkOfProgram()
        {
            Console.WriteLine("Demostrating work of program");
            Console.WriteLine("************************************");
            Console.WriteLine("Creating Polynomial with more coefficients than power");
            var firstPolynomial = new Polynomial(4, new double[] { 1, 2, 3, 4, 5, 6, 7 });
            firstPolynomial.Print();
            Console.WriteLine();

            Console.WriteLine("Creating Polynomial with less coefficients than power");
            var secondPolynomial = new Polynomial(5, new double[] { 1, 2, 3 });
            secondPolynomial.Print();
            Console.WriteLine();

            Console.WriteLine("Creating another polynomial");
            var thirdPolynomial = new Polynomial(3, new double[] { 5, 2, 8, 6 });
            thirdPolynomial.Print();
            Console.WriteLine();

            Console.WriteLine("Value of thirdPolynomial with argument = 2");
            Console.WriteLine(thirdPolynomial.CalculateValueOfArgument(2));
            Console.WriteLine();

            Console.WriteLine("Operations on two polynomials:");
            var fourthPolynomial = new Polynomial(5, new double[] { 4, 7, 8 });
            Console.WriteLine($"First polynomial: {thirdPolynomial}");
            Console.WriteLine($"Second polynomial: {fourthPolynomial}");
            Console.WriteLine($"Sum: {Polynomial.Add(thirdPolynomial, fourthPolynomial)}");
            Console.WriteLine($"Difference (first - second): {Polynomial.Substract(thirdPolynomial, fourthPolynomial)}");
            Console.WriteLine($"Difference (second - first): {Polynomial.Substract(fourthPolynomial, thirdPolynomial)}");
            Console.WriteLine($"Product: {Polynomial.Multiply(thirdPolynomial, fourthPolynomial)}");
        }
    }
}
