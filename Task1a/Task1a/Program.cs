﻿using System;

namespace Task1a
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                DemonstrateWork();
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void DemonstrateWork()
        {
            Console.WriteLine("Demonstration of work");
            Console.WriteLine("************************");
            Console.WriteLine("After creating vector of 5 elements");
            var firstVector = new Vector(new []{1,2,3,4,5});
            Console.WriteLine(firstVector + Environment.NewLine);

            Console.WriteLine("Creating vector with indeces [5, 10]");
            var secondVector = new Vector(5, 10);
            for (var i = secondVector.StartIndex; i < secondVector.EndIndex; i++)
            {
                secondVector[i] = 6;
            }

            Console.WriteLine(secondVector + Environment.NewLine);

            Console.WriteLine("After multiplying secondVector by 4");
            Console.WriteLine(secondVector.MultiplyByScalar(4) + Environment.NewLine);

            Console.WriteLine("Creating another vector with the same indeces");
            var thirdVector = new Vector(5, 10);
            for (var i = thirdVector.StartIndex; i < thirdVector.EndIndex; i++)
            {
                thirdVector[i] = 2;
            }

            Console.WriteLine(thirdVector + Environment.NewLine);

            Console.WriteLine("Result of their addition");
            Console.WriteLine(secondVector.Add(thirdVector) + Environment.NewLine);

            Console.WriteLine("Result of their substracting");
            Console.WriteLine(secondVector.Substract(thirdVector) + Environment.NewLine);

            Console.WriteLine($"Are secondVector and thirdVector equal? - {secondVector.IsEqual(thirdVector)}" + Environment.NewLine);

            Console.WriteLine($"Are secondVector and thirdVector * 3 equal? - {secondVector.IsEqual(thirdVector.MultiplyByScalar(3))}" + Environment.NewLine);

            Console.WriteLine("Trying to get secondVector[0] element");
            try
            {
                Console.WriteLine(secondVector[0]);
            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine(e.Message + Environment.NewLine);
            }

            Console.WriteLine("Trying to add secondVector and fourthVector (has other indeces)");
            var fourthVector = new Vector(1, 10);
            try
            {
                secondVector.Add(fourthVector);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
