﻿using System;
using System.Text;

namespace Task1a
{
    public class Vector
    {
        private int[] _vector;
        private int _startIndex;
        private int _endIndex;

        public Vector(int startIndex, int endIndex)
        {
            _startIndex = startIndex;
            _endIndex = endIndex;
            _vector = new int[endIndex - startIndex];
        }

        public Vector(int size) : this(0, size)
        {
        }

        public Vector(int[] array)
        {
            _startIndex = 0;
            _endIndex = array.Length;
            _vector = array;
        }

        public int Count => _vector.Length;

        public int StartIndex => _startIndex;

        public int EndIndex => _endIndex;

        public int this[int index]
        {
            get
            {
                if (index < _startIndex || index > _endIndex)
                {
                    throw new IndexOutOfRangeException("Index is out of range");
                }

                return _vector[index - _startIndex];
            }

            set
            {
                if (index < _startIndex || index > _endIndex)
                {
                    throw new IndexOutOfRangeException("Index is out of range");
                }

                _vector[index - _startIndex] = value;
            }
        }

        public Vector Add(Vector anotherVector)
        {
            return PerformOperation(anotherVector, (a, b) => a + b);
        }

        public Vector Substract(Vector anotherVector)
        {
            return PerformOperation(anotherVector, (a, b) => a - b);
        }

        public Vector MultiplyByScalar(int scalar)
        {
            var result = new Vector(_startIndex, _endIndex);
            for (var i = _startIndex; i < _endIndex; i++)
            {
                result[i] = this[i] * scalar;
            }

            return result;
        }

        public bool IsEqual(Vector anotherVector)
        {
            if (_startIndex != anotherVector._startIndex || _endIndex != anotherVector._endIndex)
            {
                throw new InvalidOperationException("Vectors with different indeces could not be compared");
            }

            for (var i = _startIndex; i < _endIndex; i++)
            {
                if (this[i] != anotherVector[i])
                {
                    return false;
                }
            }

            return true;
        }

        public override string ToString()
        {
            var stringOutput = new StringBuilder();
            for (var i = _startIndex; i < _endIndex; i++)
            {
                stringOutput.Append(this[i] + " ");
            }

            return stringOutput.ToString();
        }

        private Vector PerformOperation(Vector anotherVector, Func<int, int, int> function)
        {
            if (_startIndex != anotherVector._startIndex || _endIndex != anotherVector._endIndex)
            {
                throw new InvalidOperationException("Operation could not be performed to vectors with different indeces");
            }

            var result = new Vector(_startIndex, _endIndex);
            for (var i = _startIndex; i < _endIndex; i++)
            {
                result[i] = function(this[i], anotherVector[i]);
            }

            return result;
        }
    }
}
